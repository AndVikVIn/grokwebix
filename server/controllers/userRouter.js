const wrap = require("./wrap");
const User = require("../models/user");

module.exports.dynamicShowAllSorted = wrap(async (req,res)=>{
  let result = await User.dynamicShowAllSorted(req.query.sort,req.query.start,req.query.count);
		res.send({"data":result, "pos":req.query.start, "total_count":250});
});

module.exports.updateUser = wrap(async (req, res)=>{
  let result = await User.updateUser(req.body.id,req.body);
		res.send(result);
});
