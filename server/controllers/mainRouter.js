const express = require("express");
const router = express.Router();
const filmRoute = require("./filmRouter");
const userRoute = require("./userRouter");
const imgRouter = require("./imgRouter");

router.get("/films", filmRoute.showAll);
router.post("/films", filmRoute.updateFilm);

router.get("/users", userRoute.dynamicShowAllSorted);
router.post("/users", userRoute.updateUser);

router.post("/upload", imgRouter.upload.single("poster"), imgRouter.createImg);

module.exports = router;



