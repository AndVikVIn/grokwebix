const wrap = require("./wrap");
const Image = require("../models/img");
const multer = require("multer");

const storage = multer.diskStorage({
  destination: (req, file, cb)=>{
    cb(null, "./public/img");
  },
  filename:(req, file, cb)=>{
    cb(null, Date.now() + file.originalname);
  }
});

module.exports.upload = multer({storage: storage});

module.exports.createImg = wrap(async(req, res)=>{
  const path =req.file.path.substring(6);
  let result = await Image.createImg(req.file.filename, path);
  res.send(result);
});
