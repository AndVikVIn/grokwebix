const wrap = require("./wrap");
const Film = require("../models/film");

module.exports.showAll = wrap(async (req, res)=>{
  let result = await Film.showAll();
		res.send(result);
});

module.exports.updateFilm = wrap(async (req, res)=>{
let result = await Film.updateFilm(req.body.id, req.body);
		res.send(result);
});
