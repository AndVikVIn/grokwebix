const mongoose = require("mongoose");

const userSchema = mongoose.Schema({
	id: mongoose.Schema.Types.ObjectId,
	"Name": String,
	"Job": String,
	"Email": String
});

module.exports = userSchema;
