const mongoose = require("mongoose");

const imgSchema = mongoose.Schema({
  name: String,
  path: String
});

module.exports = imgSchema;
