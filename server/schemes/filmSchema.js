const mongoose = require("mongoose");

const filmSchema = mongoose.Schema({
	id: mongoose.Schema.Types.ObjectId,
	title: String,
	year: Number,
	votes: Number,
	rating: Number,
  rank: Number,
  post: String,
  comments: String
});

module.exports = filmSchema;
