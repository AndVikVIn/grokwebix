const mongoose = require("mongoose");
const userSchema = require("../schemes/userSchema");

const User = mongoose.model("User", userSchema);

User.dynamicShowAllSorted = async (sort, start, count)=>{
	const items = [];
	for(let key in sort){
		if(sort[key] == "asc"){
			sort[key] = 1;
		} else {sort[key] = -1;}
	}
	let amount = Number(start) + Number(count);
	const arr =	await User.find({}).sort(sort).exec();
	for (let i = start; i <= amount; i++){
		items.push(arr[i]);
	}
	return items;
};

User.updateUser = async (id, values)=>{
  let result = await User.updateOne({_id: id}, values);
  return result;
};

module.exports = User;
