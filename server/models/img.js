const mongoose = require("mongoose");
const imgSchema = require("../schemes/imgSchema");

const Image = mongoose.model("Image", imgSchema);

Image.createImg = async (name,path)=>{
  const newImage = new Image({
    name:name,
    path:path
  });
  let result = await newImage.save();
  return result;
};

module.exports = Image;
