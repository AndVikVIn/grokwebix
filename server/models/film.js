
const mongoose = require("mongoose");
const filmSchema = require("../schemes/filmSchema");

const Film = mongoose.model("Film", filmSchema);


Film.showAll = async ()=>{
	let result = await Film.find({});
	return result;
};

Film.updateFilm = async (id, values)=>{
  let result = await Film.updateOne({_id: id}, values);
  return result;	
};


module.exports = Film;
