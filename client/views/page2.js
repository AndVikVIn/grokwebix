import {JetView} from "webix-jet";

class Page2 extends JetView{
	config(){
		const datatable = {
			view:"datatable",
			localId:"table2",
			url:"/api/users",
			save:"->/api/users",
			datafetch:25,
			loadahead:25,
			editable:true,
			editaction:"click",
			columns:[
				{id:"Name", header:"Name", editor:"text", sort:"server", fillspace:true},
				{id:"Job", header:"Job", editor:"text", sort:"server", fillspace:true},
				{id:"Email", header:"Email", editor:"text", fillspace:true},
			]
		};
		return datatable;
	}
}

export default Page2;
