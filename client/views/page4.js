import {JetView} from "webix-jet";


class Page4 extends JetView{
  config(){
    const addFiles = {
      view:"uploader", 
      value:"Uppload file",
      inputName:"poster",
      localId:"fileUploader",
      inputWidth:250,
      align:"center",
      autosend:false,
      link:"mylist", 
      upload:"/api/upload",
    };
        
  
    const datatable = {
      view:"datatable",
      localId:"table4",
      editable:true,
      scroll:false,
      fixedRowHeight:false,
      rowLineHeight:15,
      rowHeight:100,
      editaction:"click",
      save:"->/api/films",
      columns:[
        {id:"year", header:"Year", editor:"text", gravity:2},
        {id:"votes", header:"Votes", editor:"text", gravity:2},
        {id:"rating", header:"Rating", editor:"text", gravity:2},
        {id:"rank", header:"Rank", editor:"text", gravity:1},
        {id:"comments", header:"Comments", fillspace:true},
        {id:"post", header:"Post", template:function(obj){
         
          if(obj.post == undefined){
           return "<img style='width:100px;height:110px' src='/img/default.jpeg'>";
          }
          return "<img style='width:100px;height:110px' src='" + obj.post +"'>";
         }, gravity:2}
      ],
      on:{
        onAfterLoad:()=>{
          this.$$("table4").adjustRowHeight("comments");
        }
      } 
    };

    const filmSelect = {
      view:"richselect",
      localId:"filmSelect", 
      name:"id",
      options:{
        body:{
        template:"#title#",
        url:"/api/films"
        }
      },
       on:{
        onChange:(id)=>{
          this.$$("table4").clearAll();
          const data = this.$$("filmSelect").getList().data.pull;
          this.$$("table4").parse(data[id]);
          this.$$("form").setValues(data[id]);
        }
      }  
    };

    const enableComments = {
      view:"checkbox",  
      label:"Enable comments?",
      labelPosition:"top", 
      value:1,
      on:{
        onChange:(val)=>{
          if(val == 1){
            this.$$("comments").show();
          } else {
            this.$$("comments").hide();
          } 
        }
      }
  };

    const comments = {
      view:"textarea",
      localId:"comments",
      attributes:{maxlength:500},
      name:"comments",
      label:"My commetns",
      labelAlign:"left",
      labelPosition:"top",
      height:200,
      placeholder:"Your comments here",
    };

    const form = {
      view:"form",
      localId:"form",
      elements:[
        {cols:[
          {rows:[
            datatable,
            {},
            { view:"list",  id:"mylist",  type:"uploader", autoheight:true, borderless:true },
            addFiles,
          ]},
          {rows:[
            filmSelect,
            enableComments,
            comments,  
            {},
            {view:"button", value:"Save", click:()=>{
             this.$$("fileUploader").send((obj=>{
               const values = this.$$("form").getValues();
               const updates = {"post":"", "comments":values.comments};
               obj ? updates["post"] = obj.path : delete updates["post"];
               this.$$("table4").updateItem(values.id, updates);
             }));

            }}
          ]}
       
        ]}
      ]
    };
    
    return form;
  }
}


export default Page4;
