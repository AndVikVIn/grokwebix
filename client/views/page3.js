import {JetView} from "webix-jet";


class Page3 extends JetView{
  config(){

    const fullInfo = {
      localId:"template",
      rows:[
        {
          view:"template",
          localId:"fullInfo",
          template:"Title - #title#<br>Year - #year#<br>Votes - #votes#<br>Rating - #rating#<br>Rank - #rank# <br>Comments - #comments#<br><img src='#post#'>",
        },
        {view:"button", value:"Close", click:()=>{
          this.$$("template").hide();
        }}
      ],
      hidden:true
    };

    const datatable = {
      view:"datatable",
      localId:"table3",
      editable:true,
      editaction:"click",
      url:"api/films",
      save:"->/api/films",
      rowHeight:100,
      columns:[
        {id:"year", header:"Year", editor:"text", fillspace:true},
				{id:"votes", header:"Votes", editor:"text", fillspace:true},
				{id:"rating", header:"Rating", editor:"text", fillspace:true},
        {id:"rank", header:"Rank", editor:"text", fillspace:true},
        {id:"post", header:"Post", width:400, template:function(obj){
         if(obj.post == undefined){
          return "<img style='width:100px;height:110px' src='/img/default.jpeg'>";
         }
          return "<img style='width:100px;height:110px' src='" + obj.post +"'>";
        }, fillspace:true}
      ],
      on:{
        onItemClick:(id)=>{
          const item = this.$$("table3").data.pull;
          this.$$("fullInfo").parse(item[id.row]);
          this.$$("template").show();
        }
      }
    };

    const list = {
      view:"list",
      localId:"list",
      template:"#title#",
      select:true,
      url:"/api/films",
      on:{
        onAfterLoad:()=>{
          const id = this.$$("list").getFirstId();
          this.$$("list").select(id);
        },
        onAfterSelect:(id)=>{
          this.$$("table3").clearAll();
          const item = this.$$("list").data.pull;
          this.$$("table3").parse(item[id]);
          this.$$("template").hide();
          
        }
      },
    };
   
    return {
      cols:[
        list,
        datatable,
        fullInfo
      ]
    };
  }
}

export default Page3;
