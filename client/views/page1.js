import {JetView} from "webix-jet";
import PopupWin from "./popupWin";

class Page1 extends JetView{
  config(){
    const toolbar = {
      view:"toolbar",
      cols:[
        {view:"button", value:"Export to excel", type:"form", width:200, align:"left", click:()=>{
          webix.toExcel(this.$$("table1"), {
            columns:[
              {id:"title", header:"Title"},
              {id:"year", header:"Year"},
              {id:"votes", header:"Votes"},
              {id:"rating", header:"Rating"},
              {id:"rank", header:"Rank"}
            ]
          });
        }},
        {view:"button", value:"Refresh", type:"form", width:100, align:"left", click:()=>{
          this.$$("table1").load("/api/films").then((res)=>{
            this.$$("table1").parse(res);
          });
        }}
      ]
    };

		const datatable = {
			view:"datatable",
			localId:"table1",
			url:"/api/films",
      save:"->/api/films",
      rowHeight:100,
			columns:[
				{id:"title", header:["Title", {content:"textFilter"}], sort:"string", fillspace:true},
				{id:"year", header:["Year", {content:"numberFilter"}], sort:"int"},
				{id:"votes", header:["Votes", {content:"numberFilter"}], sort:"int"},
				{id:"rating", header:["Rating", {content:"numberFilter"}], sort:"int"},
        {id:"rank", header:["Rank", {content:"numberFilter"}], sort:"int"},
        {id:"post", header:"Poster", width:400, template:function(obj){
          if(obj.post == undefined){
           return "<img style='width:100px;height:110px' src='/img/default.jpeg'>";
          }
           return "<img style='width:100px;height:110px' src='" + obj.post +"'>";
         }, fillspace:true}
			],
			on:{
				onItemClick:(id)=>{
					const item = this.$$("table1").getItem(id);
					this.PopupWin.showPopup(item);
				}
			}
		};
	
		return {
			rows:[
				toolbar,
				datatable
			]
		};
	}
	init(){
		this.PopupWin = this.ui(PopupWin);
		this.on(this.app, "updateCurrentItem", (id, values)=>{
			this.$$("table1").updateItem(id, values);
		});
	}
}

export default Page1;
