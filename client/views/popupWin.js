import {JetView} from "webix-jet";

class PopupWin extends JetView{
	config(){
		return{
			view:"popup",
			position:"center", height:600, width:700,
			body:{
				view:"form",
				localId:"form",
				elements:[
					{type:"header", template:"Edit row"},
					{view:"text", label:"Title", attributes:{maxlength:80}, name:"title", invalidMessage:"Can't be empty"},
					{view:"text", label:"Year", name:"year", invalidMessage:"Can't be empty"},
					{view:"text", label:"Votes", name:"votes", invalidMessage:"Can't be empty"},
					{view:"text", label:"Rating", name:"rating", invalidMessage:"Can't be empty"},
					{
						cols:[
							{view:"button", value:"Save", click:()=>{
								if(this.$$("form").validate()){
                  const values = this.$$("form").getValues();
									this.app.callEvent("updateCurrentItem", [values.id, values]);
									this.getRoot().hide();
								}
							}},
							{view:"button", value:"Cancel", click:()=>{
								this.$$("form").clear();
								this.getRoot().hide();
							}},
						]
					}
				],
				rules:{
					$all:webix.rules.isNotEmpty
				}
			}
		};
	}
	showPopup(item){
		this.$$("form").setValues(item);
		this.getRoot().show();
	}
}

export default PopupWin;
